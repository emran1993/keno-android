package com.emran.emransheikhah.keno;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> data = new ArrayList<String>();
    Button generateNumbers;
    CheckBox setMode7Num;
    Random pickNumber = new Random();
    boolean is7NumEnabled = false;
    CheckBox setMode17Num;
    Integer numberOfLines = 0;
    boolean payEmran = false;
    Th adsTh ;
    String monAdId = "";
    String bannerAdId = "";
    String initAdId = "";
    boolean canShowAd = false;
    boolean payEmran2 = false;
    private LinearLayout mAdViewContainer;
    TextView b1;
    TextView b2;
    TextView b3;
    TextView b4;
    TextView b5;
    TextView b6;
    TextView b21;
    TextView b22;
    TextView b23;
    TextView b24;
    TextView b25;
    TextView b26;
    TextView b321;
    TextView b322;
    TextView b323;
    TextView b324;
    TextView b325;
    TextView b326;
    TextView details;
    InterstitialAd mInterstitialAd;
    ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        initTextView();

        b326.setVisibility(View.GONE);

        payEmran = pickNumber.nextBoolean();
        payEmran2 = pickNumber.nextBoolean();

        if (payEmran){
            if (payEmran2){

                monAdId = "ca-app-pub-8527602727531000~8299586958";
                bannerAdId = "ca-app-pub-8527602727531000/6603361903";
                initAdId = "ca-app-pub-8527602727531000/8868818192";
            }else  {

                monAdId = "ca-app-pub-6836066066475364~1803580923";
                bannerAdId = "ca-app-pub-6836066066475364/4827245804";
                initAdId = "ca-app-pub-6836066066475364/1378955389";
            }
        }else { // pay roy

            monAdId = "ca-app-pub-6953273335891067~2472830614";
            bannerAdId = "ca-app-pub-6953273335891067/7533585605";
            initAdId = "ca-app-pub-6953273335891067/2281258929";
        }



        MobileAds.initialize(this, monAdId);

        mAdViewContainer = (LinearLayout) findViewById(R.id.adViewContainer);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.LARGE_BANNER);
        adView.setAdUnitId(bannerAdId);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        LinearLayout.LayoutParams _lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if(adView.getParent()!=null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }
        mAdViewContainer.addView(adView,_lp);



        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(initAdId);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });


        numberOfLines = 0;

        generateNumbers = findViewById(R.id.button);
        setMode7Num = findViewById(R.id.btnum7);
        setMode17Num = findViewById(R.id.btnum17);


        if (data.isEmpty()){
            new getData().execute();
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("טוען מידע...");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
        adsTh = new Th();



    }

    @Override
    public void onStart(){
        super.onStart();
        canShowAd = false;
        try{
            adsTh.start();
        }catch (Exception e){
            adsTh = new Th();
            adsTh.start();
        }



    }

    @Override
    public void onStop(){
        super.onStop();
        adsTh.interrupt();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);

    }



    public  void initTextView(){

        b1 = findViewById(R.id.t11);
        b2 = findViewById(R.id.t12);
        b3 = findViewById(R.id.t13);
        b4 = findViewById(R.id.t14);
        b5 = findViewById(R.id.t15);
        b6 = findViewById(R.id.t16);

        b21 = findViewById(R.id.t21);
        b22 = findViewById(R.id.t22);
        b23 = findViewById(R.id.t23);
        b24 = findViewById(R.id.t24);
        b25 = findViewById(R.id.t25);
        b26 = findViewById(R.id.t26);

        b321 = findViewById(R.id.t31);
        b322 = findViewById(R.id.t32);
        b323 = findViewById(R.id.t33);
        b324 = findViewById(R.id.t34);
        b325 = findViewById(R.id.t35);
        b326 = findViewById(R.id.t45);
        details = findViewById(R.id.details);
    }



    private class getData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            data.addAll(readData("k1"));
            data.addAll(readData("k2"));
            data.addAll(readData("k3"));
            data.addAll(readData("k4"));
            data.addAll(readData("k5"));
            numberOfLines = data.size();
            return null;

         }

        @Override
        protected void onPostExecute(Void v) {
             setMode17Num.setChecked(true);
            show17Numbers(getNumbers());
            mProgressDialog.cancel();
        }

    }

    class Th extends Thread {

        @Override
        public void run(){
            while (true){
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                canShowAd = true;
            }

        }

    }

   public void generateNumbersClicked(View v){

        int showAd = pickNumber.nextInt(44);
        if (mInterstitialAd.isLoaded() && showAd%11 == 0 && canShowAd){
            mInterstitialAd.show();
            canShowAd = false;
        }else  if (!mInterstitialAd.isLoaded()){
            requestNewInterstitial();
        }
        if (is7NumEnabled) {
            show7Numbers(getNumbers());

       }else {

            show17Numbers(getNumbers());

        }


    }

    public  String[] getNumbers(){
        Integer randomLineIndex = pickNumber.nextInt(numberOfLines-1);
        String[] newNumbers = data.get(randomLineIndex).split(",");
        return  newNumbers;

    }

    public void btnClicked(View v){

        if (v.getId() == setMode7Num.getId()){
            is7NumEnabled = true;
            show7Numbers(getNumbers());
            setMode17Num.setChecked(false);
            setMode7Num.setChecked(true);

        }else if (v.getId() == setMode17Num.getId()){

            is7NumEnabled = false;
            show17Numbers(getNumbers());
            setMode7Num.setChecked(false);
            setMode17Num.setChecked(true);

        }

    }


    private void needToHideViews(boolean hide) {

        int visible = hide ? View.GONE : View.VISIBLE;;
        b1.setVisibility(visible);
        b5.setVisibility(visible);
        b6.setVisibility(visible);

        b21.setVisibility(visible);
        b25.setVisibility(visible);
        b26.setVisibility(visible);

        b321.setVisibility(visible);
        b325.setVisibility(visible);
        //b26.setVisibility(visible);
        int line3Vis = visible == View.GONE ? View.INVISIBLE
                : visible;
        b322.setVisibility(line3Vis);
        b324.setVisibility(line3Vis);
        b325.setVisibility(line3Vis);
       b321.setVisibility(line3Vis);
       b326.setVisibility(visible);



    }

    public  void show17Numbers(String[] numbers){

        needToHideViews(false);

        b1.setText(numbers[2]);
        b2.setText(numbers[3]);
        b3.setText(numbers[4]);
        b4.setText(numbers[5]);
        b5.setText(numbers[6]);
        b6.setText(numbers[7]);

        b21.setText(numbers[8]);
        b22.setText(numbers[9]);
        b23.setText(numbers[10]);
        b24.setText(numbers[11]);
        b25.setText(numbers[12]);
        b26.setText(numbers[13]);

        b321.setText(numbers[14]);
        b322.setText(numbers[15]);
        b323.setText(numbers[16]);
        b324.setText(numbers[17]);
        b325.setText(numbers[18]);

        details.setText("הצירוף שהוגרל זכה בתאריך "+ numbers[1]);

    }

    public  void show7Numbers(String[] numbers){

        needToHideViews(true);
        ArrayList<Integer> randomIndexs = new ArrayList<>();
        int index_1 =  pickNumber.nextInt(15)+2;
        randomIndexs.add(index_1);
        int index_2 = pickNumber.nextInt(15)+2;
        while (index_1 == index_2) {
            index_2 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_2);
        int index_3 = pickNumber.nextInt(15)+2;
        while (index_1 == index_3 || index_2 == index_3) {
            index_3 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_3);

        int index_4 = pickNumber.nextInt(15)+2;
        while (index_1 == index_4 || index_2 == index_4 || index_3 == index_4) {
            index_4 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_4);
               int index_5 = pickNumber.nextInt(15)+2;
        while (index_1 == index_5 || index_2 == index_5 || index_3 == index_5 || index_4 == index_5) {
            index_5 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_5);
        int index_6 = pickNumber.nextInt(15)+2;
        while (index_1 == index_6 || index_2 == index_6 || index_3 == index_6 || index_4 == index_6 || index_5 == index_6) {
            index_6 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_6);
        int index_7 = pickNumber.nextInt(15)+2;
        while (index_1 == index_7 || index_2 == index_7 || index_3 == index_7 || index_4 == index_7 || index_5 == index_7 || index_6 == index_7) {
            index_7 = pickNumber.nextInt(15)+2;
        }
        randomIndexs.add(index_7);


        Collections.sort(randomIndexs);

        b2.setText(numbers[randomIndexs.get(6)]);
        b3.setText(numbers[randomIndexs.get(5)]);
        b4.setText(numbers[randomIndexs.get(4)]);

        b22.setText(numbers[randomIndexs.get(3)]);
        b23.setText(numbers[randomIndexs.get(2)]);
        b24.setText(numbers[randomIndexs.get(1)]);
        b323.setText(numbers[randomIndexs.get(0)]);

        details.setText("הצירוף שהוגרל זכה בתאריך "+ numbers[1]);


    }






    public  ArrayList<String> readData(String filenme){

        BufferedReader reader;
        ArrayList<String> lines = new ArrayList<String>();
        try{
            final InputStream file = getAssets().open(filenme);
            reader = new BufferedReader(new InputStreamReader(file));
            String line = reader.readLine();


            while(line != null){
                lines.add(line.replace("\"",""));
                line = reader.readLine();

            }

            return lines;
        } catch(IOException ioe){
            ioe.printStackTrace();
        }

        return  lines;
    }
}
